/**
 * Database Key.
 *
 * @description This class is optionally used as a object to pass to the `Database::Create` method when creating a new table.
 *              It contains data making it easier to grasp the intended structure of the keys in the table.
 *              If not used, the passed object must follow the same specification.
 *
 * @class
 *
 * @package   @jitesoft/dbal
 * @copyright Jitesoft 2019
 * @license   MIT
 *
 * @property {String}  Name         Name of the key.
 * @property {String}  Type         Key type as string.
 * @property {Boolean} IsPrimary    If it is a primary index.
 * @property {Boolean} IsUnique     If the key is unique in the table.
 * @property {Boolean} IsNullable   If value can be null.
 * @property {*}       DefaultValue Value which is default if not set.
 */
export default class DatabaseKey {
  /**
   *
   * @param {String}  name           Name of the key.
   * @param {String}  type           Key type as string.
   * @param {Boolean} [isPrimary]    If it is a primary index.
   * @param {Boolean} [isUnique]     If the key is unique in the table.
   * @param {Boolean} [isNullable]   If value can be null.
   * @param {*}       [defaultValue] Value which is default if not set.
   */
  constructor (name, type, isPrimary = false, isUnique = false, isNullable = true, defaultValue = null) {
    this._name = name;
    this._type = type;
    this._isPrimary = isPrimary;
    this._isUnique = isUnique;
    this._isNullable = isNullable;
    this._defaultValue = defaultValue;
  }

  /**
   * @return {String}
   */
  get Name () {
    return this._name;
  }

  /**
   * @return {String}
   */
  get Type () {
    return this._type;
  }

  /**
   * @return {Boolean}
   */
  get IsPrimary () {
    return this._isPrimary;
  }

  /**
   * @return {Boolean}
   */
  get IsUnique () {
    return this._isUnique;
  }

  /**
   * @return {Boolean}
   */
  get IsNullable () {
    return this._isNullable;
  }

  /**
   * @return {*}
   */
  get DefaultValue () {
    return this._defaultValue;
  }
}
