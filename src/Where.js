/**
 * Where.
 *
 * @description This class is intended to represent a where clause.
 *              It is optionally used in queries that requires a Where clause.
 *              If not used, the object passed must follow the same standard.
 *
 * @class
 *
 * @package   @jitesoft/dbal
 * @copyright Jitesoft 2019
 * @license   MIT
 *
 * @property {String}     Key       Key to test.
 * @property {String}     Condition Condition to test with.
 * @property {*|Array<*>} Value     Value or values to test against.
 */
export default class Where {
  /**
   * @param {String}     key       Key to test.
   * @param {String}     condition Condition.
   * @param {*|Array<*>} value     Value or values to test against.
   */
  constructor (key, condition, value) {
    this._key = key;
    this._condition = condition;
    this._value = value;
  }

  /**
   * @return {String}
   */
  get Key () {
    return this._key;
  }

  /**
   * @return {String}
   */
  get Condition () {
    return this._condition;
  }

  /**
   * @return {*}
   */
  get Value () {
    return this._value;
  }
}
