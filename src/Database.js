import Where from './where.js';
import Key from './Key.js';

/**
 * Database interface.
 *
 * @description This class is intended to represent a single database once implemented.
 *              Each implementation have to implement the `abstract` marked methods to be able to function properly.
 *
 * @abstract
 * @class
 *
 * @package   @jitesoft/dbal
 * @copyright Jitesoft 2019
 * @license   MIT
 */
export default class Database {
  /**
   * Create a new database table.
   *
   * @param {String}     table  Table name.
   * @param {Array<Key>} keys   Database keys.
   *
   * @return {Promise<Boolean>} True on success.
   *
   * @abstract
   * @access public
   */
  async create (table, keys) {
    throw new Error('Abstract method.');
  }

  /**
   * Insert an object into the database.
   *
   * @param {String}                             table  Table name.
   * @param {Array<String>|Array<Array<String>>} keys   Keys to set values to.
   * @param {Array<*>|Array<Array<*>>}           values Values to set.
   *
   * @return {Promise<Boolean>} True on success.
   *
   * @abstract
   * @access public
   */
  async insert (table, keys, values) {
    throw new Error('Abstract method.');
  }

  /**
   * Select one or many objects from the database.
   *
   * @param {String}                  table Table name.
   * @param {Array<Where>|Where|null} where Where clauses.
   *
   * @return {Promise<Array<*>>} Objects selected.
   *
   * @abstract
   * @access public
   */
  async select (table, where = null) {
    throw new Error('Abstract method.');
  }

  /**
   * Update one or many objects in the database.
   *
   * @param {String}                  table  Table name.
   * @param {Array<String>}           keys   Keys to update.
   * @param {Array<String>}           values Values to set.
   * @param {Array<Where>|Where|null} where Where clauses.
   *
   * @return {Promise<Boolean>} True on success.
   *
   * @abstract
   * @access public
   */
  async update (table, keys, values, where = null) {
    throw new Error('Abstract method.');
  }

  /**
   * Delete one or many objects from the database.
   *
   * @param {String}                  table Table name.
   * @param {Array<Where>|Where|null} where Where clauses.
   *
   * @return {Promise<Boolean>} True on success.
   *
   * @abstract
   * @access public
   */
  async delete (table, where = null) {
    throw new Error('Abstract method.');
  }
}
