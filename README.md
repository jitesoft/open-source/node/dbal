# @jitesoft/dbal

[![npm (scoped)](https://img.shields.io/npm/v/@jitesoft/dbal)](https://www.npmjs.com/package/@jitesoft/dbal)
[![Known Vulnerabilities](https://dev.snyk.io/test/npm/@jitesoft/dbal/badge.svg)](https://dev.snyk.io/test/npm/@jitesoft/dbal)
[![pipeline status](https://gitlab.com/jitesoft/open-source/node/dbal/badges/master/pipeline.svg)](https://gitlab.com/jitesoft/open-source/node/dbal/commits/master)

Super simple database abstraction layer for nodejs.

## Intention

This project is intended as an interface for very very simple db wrappers.  
The only required methods to implement for a new implementation are the following:

* create (table)
* insert (one or many)
* select (one or many)
* update (one or many)
* delete (one or many)

That means, the `database` does not have to be a full database engine, rather just a single database in which tables can
be manipulated.

Currently, no implementations exists, only the abstract interface, which is intended to be used with WebPack and `import`.
